'use strict';

/**
 * @ngdoc overview
 * @name shopApp
 * @description
 * # shopApp
 *
 * Main module of the application.
 */
angular
  .module('shopApp', [
    'ngAnimate',
    'ngRoute',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
